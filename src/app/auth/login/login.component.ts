import { Component } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { HttpErrorResponse } from '@angular/common/http';
interface LoginResponseModel {
  entry: TokenModel;
}
interface TokenModel {
  id: string;
  userId: string;
}
@Component({
  selector: 'am-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public loginForm: FormGroup;
  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    public snackBar: MatSnackBar
  ) {
    this.loginForm = this.fb.group({
      userId: ['', Validators.required],
      password: ['', Validators.required]
    });
  }
  onSubmit() {
    this.loginService.login(this.loginForm.value).subscribe((response: LoginResponseModel) => {
      if (response.entry && response.entry.id) {
        localStorage.setItem('token', response.entry.id);
        localStorage.setItem('user', response.entry.userId);
        this.router.navigate(['/people']);
      }
    }, (response: HttpErrorResponse) => {
      // response.error.error.errorKey not so nice ;)
      this.snackBar.open(response.error.error.errorKey, 'Ok', {
        duration: 2000,
      });
    });
  }
}
