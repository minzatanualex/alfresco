import { environment } from './../../../environments/environment';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})

 /**
  * Login Service should extend a base service ...
  */
export class LoginService {

  constructor(private http: HttpClient) { }

  login(credentials: User) {
    const url = environment.alfrescoAuthApi + 'tickets';
    return this.http.post(url, credentials);
  }
}
