import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {

  constructor(private http: HttpClient) { }

  checkTicket(ticket: any) {
    const url = environment.alfrescoAuthApi + 'tickets/' + ticket;
    return this.http.get(url);
  }
}
