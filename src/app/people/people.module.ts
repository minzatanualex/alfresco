import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatCardModule} from '@angular/material/card';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import { PeopleRoutingModule } from './people-routing.module';
import { FavoritesComponent } from './favorites/favorites.component';
import { FavoritesService } from './favorites/favorites.service';
const materialModules = [MatCardModule,
  MatSnackBarModule];
@NgModule({
  imports: [
    CommonModule,
    PeopleRoutingModule,
    ...materialModules
  ],
  providers: [FavoritesService],
  declarations: [FavoritesComponent]
})
export class PeopleModule { }
