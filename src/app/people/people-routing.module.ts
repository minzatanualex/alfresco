import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FavoritesComponent } from './favorites/favorites.component';

const routes: Routes = [
  { path: '',
    redirectTo: 'favorites',
    pathMatch: 'full'
  },
  { path: 'favorites', component: FavoritesComponent },
  { path: '**',
    redirectTo: 'favorites',
    pathMatch: 'full'
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PeopleRoutingModule { }
