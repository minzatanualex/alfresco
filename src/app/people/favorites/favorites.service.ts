import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class FavoritesService {

  constructor(private http: HttpClient) { }

  getFavorites() {
    const url = environment.alfrescoPeopleApi + localStorage.getItem('user') + '/favorites';
    //  request.setRequestHeader ("Authorization", "Basic " + btoa(ticket));
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Basic ' + btoa(localStorage.getItem('token'))
      })
  };
  return this.http.get(url , httpOptions);
}
}
