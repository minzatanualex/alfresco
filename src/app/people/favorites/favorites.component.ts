import { HttpErrorResponse } from '@angular/common/http';
import { FavoritesService } from './favorites.service';
import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { FavoritesList } from './favorites.model';

@Component({
  selector: 'am-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.scss']
})
export class FavoritesComponent implements OnInit {
 public favoritesList: any = [];
  constructor( public snackBar: MatSnackBar, private router: Router, private favoritesService: FavoritesService) { }

  ngOnInit() {
   if (localStorage.getItem('user')) {
    this.favoritesService.getFavorites().subscribe((response: FavoritesList) => {
      this.favoritesList = response.list.entries;
    }, (response: HttpErrorResponse) => {
      this.snackBar.open(response.error.error.errorKey, 'Ok', {
        duration: 2000,
      });
    });
   } else {
     this.router.navigate(['/login']);
   }
  }

}
