import { FavoritesService } from './favorites.service';
import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { environment } from '../../../environments/environment';

describe('FavoritesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [FavoritesService]
    });
  });

  it(
    'should be initialized',
    inject([FavoritesService], (favoritesService: FavoritesService) => {
      expect(favoritesService).toBeTruthy();
    })
  );

  it(
    'should have getFavorites()',
    inject(
      [FavoritesService, HttpTestingController],
      (favoritesService: FavoritesService, backend: HttpTestingController) => {
        expect(favoritesService.getFavorites).toBeTruthy();
      }
    )
  );
});
