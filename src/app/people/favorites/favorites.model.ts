export interface FavoritesList {
  list: ListProps;
}
interface ListProps {
  pagination: Pagination;
  entries: EntriesList[];
}

interface EntriesList {
  entry: Entry;
}
interface Pagination {
  count: number;
  hasMoreItems: boolean;
  totalItems: number;
  skipCount: number;
  maxItems: number;
}
interface Entry {
  targetGuid: string;
  createdAt: string;
  target: Target;
}

interface Target {
  site: Site;
}

interface Site {
    id: string;
    guid: string;
    title: string;
    description: string;
    visibility: string;
    preset: string;
    role: string;
  }
