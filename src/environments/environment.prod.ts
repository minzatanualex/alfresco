export const environment = {
  production: true,
  publicApi: 'https://api-explorer.alfresco.com/alfresco/api/-default-/public/',
  alfrescoAuthApi: 'https://api-explorer.alfresco.com/alfresco/api/-default-/public/authentication/versions/1/',
  alfrescoPeopleApi: 'https://api-explorer.alfresco.com/alfresco/api/-default-/public/alfresco/versions/1/people/'
};
