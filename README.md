Alfresco Client App with **login** page and **favorites** pages.
Author: Alexandru Minzatanu

Run: 
 In main project folder:
    - open terminal (cmd/powershel on windows)
	  - run: npm install (nmp i) (nodejs is required to run this command (https://nodejs.org/en/))
	  - run: npm serve (a development server should start)
	  - open browser and go to http://localhost:4200

Navigation:

When you go to http://localhost:4200 you will be redirected to http://localhost:4200/login
User: admin and Password: admin
If login request return a token you will be redirected to http://localhost:4200/people/favorites
where you can see the favorites list of files and folders for the current user.
